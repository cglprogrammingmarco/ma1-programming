#include <Game/Ball.hpp>
#include <Game/Maths.hpp>
#include <iostream>

picker::Ball::Ball()
{
	_circleShape = sf::CircleShape(25.0f);
	_circleShape.setOrigin(_circleShape.getRadius() / 2.0f, _circleShape.getRadius() / 2.0f);

	setVelocity(sf::Vector2f(0, 0));
}

void picker::Ball::update(float deltaTime, const sf::Vector2u& screenSize, const picker::PlayerContainer<picker::SharedObject<picker::Paddle>>& paddles)
{
	const sf::Vector2f& position = _circleShape.getPosition();
	const float radius = _circleShape.getRadius();
	
	bool isBallTouchingTop = position.y - radius <= 0.0f;
	bool isBallTouchingBottom = position.y + radius >= screenSize.y;

	bool isBallTouchingRightSide = position.x + radius >= screenSize.x;
	bool isBallTouchingLeftSide = position.x - radius <= 0.0f;

	if(isBallTouchingTop || isBallTouchingBottom)
	{
		_currentVelocity.y *= -1.0f;
	}

	if(isBallTouchingLeftSide || isBallTouchingRightSide)
	{
		reset(screenSize);
		_currentVelocity *= -1.0f;
	}

	const sf::FloatRect& ballBounds = _circleShape.getGlobalBounds();
	const sf::FloatRect& paddleABounds = paddles.PlayerA.get().getBounds();
	const sf::FloatRect& paddleBBounds = paddles.PlayerB.get().getBounds();
	
	if(ballBounds.intersects(paddleABounds) || ballBounds.intersects(paddleBBounds))
	{
		_currentVelocity.x *= -1.0f;
	}

	// Move
	setPosition(_circleShape.getPosition() + _currentVelocity * deltaTime * 300.0f);
}

void picker::Ball::reset(const sf::Vector2u& screenSize)
{
	const sf::Vector2f middleOfScreen(screenSize.x / 2.0f, screenSize.y / 2.0f);
	_circleShape.setPosition(middleOfScreen);
}

void picker::Ball::setPosition(const sf::Vector2f& newPosition)
{
	_circleShape.setPosition(newPosition);
}

void picker::Ball::setVelocity(const sf::Vector2f& newVelocity)
{
	_currentVelocity = picker::Maths::normalize(newVelocity);
}

const sf::Vector2f& picker::Ball::getVelocity() const
{
	return _currentVelocity;
}

sf::FloatRect picker::Ball::getBounds() const
{
	return _circleShape.getGlobalBounds();
}

void picker::Ball::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(_circleShape);
}
