#ifndef PICKER_MA1_PROGRAMMING_THREADING_GAME_HPP
#define PICKER_MA1_PROGRAMMING_THREADING_GAME_HPP

#include <SFML/Graphics.hpp>
#include <Game/Paddle.hpp>
#include <Game/PlayerInput.hpp>
#include <Game/Ball.hpp>
#include <Game/PlayerContainer.hpp>
#include <Threading/FrequencyThread.hpp>
#include <Threading/SharedObject.hpp>

namespace picker
{
	class Game final
	{
	public:
		Game(sf::RenderWindow& renderWindow);

		void start();
		bool isRunning() const;
		void requestStop();

		sf::RenderWindow& getRenderWindow();
		

	private:
		bool _isRunning;
		sf::RenderWindow& _renderWindow;
		
		picker::FrequencyThread _inputHandlingThread;
		picker::FrequencyThread _gameUpdateThread;
		picker::FrequencyThread _renderingThread;
		
		picker::PlayerContainer<picker::SharedObject<picker::Paddle>> _paddleContainer;
		picker::PlayerContainer<picker::SharedObject<picker::PlayerInput>> _playerInputContainer;
		picker::SharedObject<picker::Ball> _ballContainer;

		picker::PlayerInput createPlayerInputForKeys(sf::Keyboard::Key up, sf::Keyboard::Key down);

		void setUpGameObjects();
		void handleInputThreadIteration(float deltaTime);
		void gameUpdateThreadIteration(float deltaTime);
		void renderingThreadIteration(float deltaTime);
	};
}

#endif // !PICKER_MA1_PROGRAMMING_THREADING_GAME_HPP