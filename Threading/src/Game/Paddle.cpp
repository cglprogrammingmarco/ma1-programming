#include <Game/Paddle.hpp>

picker::Paddle::Paddle(const sf::Vector2u& windowDimensions, bool isLeftPlayer)
{
	const float width = 15;
	const float height = windowDimensions.y / 3.0f;
	const sf::Vector2f dimensions(width, height);
	
	_rectangleShape.setSize(dimensions);
	_rectangleShape.setFillColor(sf::Color::White);

	_rectangleShape.setOrigin(width / 2.0f, height / 2.0f);

	const float distanceFromLeft = isLeftPlayer ? 25.0f : windowDimensions.x - 25.0f;
	_rectangleShape.setPosition(distanceFromLeft, windowDimensions.y / 2.0f);
}

void picker::Paddle::moveOnYAxis(float delta)
{
	const sf::Vector2f& previousPosition = _rectangleShape.getPosition();
	_rectangleShape.setPosition(previousPosition.x, previousPosition.y + delta * 2.5f);
}

sf::FloatRect picker::Paddle::getBounds() const
{
	return _rectangleShape.getGlobalBounds();
}

void picker::Paddle::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(_rectangleShape, states);
}
