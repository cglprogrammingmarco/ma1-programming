#ifndef MATHS_HPP
#define MATHS_HPP

#include <SFML/Graphics.hpp>

namespace picker
{
	static class Maths
	{
	public:
		static sf::Vector2f normalize(const sf::Vector2f& v)
		{
			const float lengthSquared = v.x * v.x + v.y * v.y;
			const float length = sqrt(lengthSquared);
			return v / length;
		}
	};
}

#endif // !MATHS_HPP