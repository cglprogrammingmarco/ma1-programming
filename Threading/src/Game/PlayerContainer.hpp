#ifndef PICKER_MA1_PROGRAMMING_PONK_SRC_GAME_PLAYER_CONTAINER_HPP 
#define PICKER_MA1_PROGRAMMING_PONK_SRC_GAME_PLAYER_CONTAINER_HPP

#include <stdexcept>
#include <sstream>

namespace picker
{
	template<typename T>
	struct PlayerContainer
	{
	public:
		PlayerContainer() = default;
		PlayerContainer(const T& a, const T& b);

		T& operator[](const int index);

		T PlayerA;
		T PlayerB;
	};
}

template<typename T>
picker::PlayerContainer<T>::PlayerContainer(const T& a, const T& b)
{
	PlayerA = a;
	PlayerB = b;
}

template<typename T>
T& picker::PlayerContainer<T>::operator[](const int index)
{
	switch(index)
	{
		case 0:
			return PlayerA;
		case 1:
			return PlayerB;
		default:
		{
			std::stringstream ss;
			ss << "The index " << index << "is out of range! Allowed indices are: [0, 1].";
			throw std::out_of_range(ss.str());
		}
	}
}

#endif // !PICKER_MA1_PROGRAMMING_PONK_SRC_GAME_PLAYER_CONTAINER_HPP
