#ifndef PICKER_MA1_PROGRAMMING_PONK_SRC_GAME_BALL_HPP
#define PICKER_MA1_PROGRAMMING_PONK_SRC_GAME_BALL_HPP

#include <Game/PlayerContainer.hpp>
#include <Game/Paddle.hpp>
#include <Threading/SharedObject.hpp>

#include <SFML/Graphics.hpp>

namespace picker
{
	class Ball : public sf::Drawable
	{
	public:
		Ball();

		void update(float deltaTime, const sf::Vector2u& screenSize, const picker::PlayerContainer<picker::SharedObject<picker::Paddle>>& paddles);
		void reset(const sf::Vector2u& screenSize);
		void setPosition(const sf::Vector2f& newPosition);
		void setVelocity(const sf::Vector2f& newVelocity);
		const sf::Vector2f& getVelocity() const;
		sf::FloatRect getBounds() const;

	private:
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

		sf::CircleShape _circleShape;
		sf::Vector2f _currentVelocity;
	};
}

#endif // !PICKER_MA1_PROGRAMMING_PONK_SRC_GAME_BALL_HPP