#ifndef PICKER_MA1_PROGRAMMING_PONK_SRC_GAME_PADDLE_HPP
#define	PICKER_MA1_PROGRAMMING_PONK_SRC_GAME_PADDLE_HPP

#include <SFML/Graphics.hpp>

namespace picker
{
	class Paddle : public sf::Drawable
	{
	public:
		Paddle() = default;
		Paddle(const sf::Vector2u& windowDimensions, bool isLeftPlayer );

		void moveOnYAxis(float delta);
		sf::FloatRect getBounds() const;

	private:
		sf::RectangleShape _rectangleShape;
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	};
}

#endif // !PICKER_MA1_PROGRAMMING_PONK_SRC_GAME_PADDLE_HPP