#include <Game/Game.hpp>
#include <functional>
#include <iostream>

picker::Game::Game(sf::RenderWindow& renderWindow) :
	_renderWindow(renderWindow),
	_isRunning(false),
	_inputHandlingThread(std::bind(&picker::Game::handleInputThreadIteration, this, std::placeholders::_1), 30.0f),
	_gameUpdateThread(std::bind(&picker::Game::gameUpdateThreadIteration, this, std::placeholders::_1), 60.0f),
	_renderingThread(std::bind(&picker::Game::renderingThreadIteration, this, std::placeholders::_1), 60.0f)
{
	setUpGameObjects();
}

void picker::Game::start()
{
	_isRunning = true;

	_inputHandlingThread.start();
	_gameUpdateThread.start();
	_renderingThread.start();
}

bool picker::Game::isRunning() const
{
	return _isRunning;
}

void picker::Game::requestStop()
{
	_isRunning = false;
	_inputHandlingThread.requestStop();
	_gameUpdateThread.requestStop();
	_renderingThread.requestStop();
}

void picker::Game::handleInputThreadIteration(float deltaTime)
{
	picker::PlayerInput playerAInput = createPlayerInputForKeys(sf::Keyboard::Key::W, sf::Keyboard::Key::S);
	picker::PlayerInput playerBInput = createPlayerInputForKeys(sf::Keyboard::Key::Up, sf::Keyboard::Key::Down);

	_playerInputContainer.PlayerA.set(playerAInput);
	_playerInputContainer.PlayerB.set(playerBInput);
}

picker::PlayerInput picker::Game::createPlayerInputForKeys(sf::Keyboard::Key up, sf::Keyboard::Key down)
{
	picker::PlayerInput input;

	if(sf::Keyboard::isKeyPressed(up))
	{
		input.inputAxes.y = -1;
	}
	else if(sf::Keyboard::isKeyPressed(down))
	{
		input.inputAxes.y = 1;
	}

	return input;
}

void picker::Game::gameUpdateThreadIteration(float deltaTime)
{
	for(int i = 0; i < 2; i++)
	{
		const PlayerInput& input = _playerInputContainer[i].get();
		_paddleContainer[i].get().moveOnYAxis(input.inputAxes.y * deltaTime * 100.0f);
	}

	_ballContainer.get().update(deltaTime, getRenderWindow().getSize(), _paddleContainer);
}

void picker::Game::renderingThreadIteration(float deltaTime)
{
	if(_renderWindow.isOpen() == false)
	{
		return;
	}

	_renderWindow.setActive(true);
	_renderWindow.clear();

	// Render paddles.
	for(int i = 0; i < 2; i++)
	{
		const Paddle& paddle = _paddleContainer[i].get();
		_renderWindow.draw(paddle);
	}

	_renderWindow.draw(_ballContainer.get());

	_renderWindow.display();
}

sf::RenderWindow& picker::Game::getRenderWindow()
{
	return _renderWindow;
}

void picker::Game::setUpGameObjects()
{
	picker::Paddle playerAPaddle = picker::Paddle(getRenderWindow().getSize(), true);
	picker::Paddle playerBPaddle = picker::Paddle(getRenderWindow().getSize(), false);

	_paddleContainer.PlayerA.set(playerAPaddle);
	_paddleContainer.PlayerB.set(playerBPaddle);

	_ballContainer.get().reset(getRenderWindow().getSize());
	_ballContainer.get().setVelocity(sf::Vector2f(1.0f, 1.0f));
}
