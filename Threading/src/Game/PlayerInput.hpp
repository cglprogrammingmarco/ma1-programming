#ifndef PICKER_MA1_PROGRAMMING_THREADING_PLAYERINPUT_HPP
#define PICKER_MA1_PROGRAMMING_THREADING_PLAYERINPUT_HPP

#include <SFML/System/Vector2.hpp>

namespace picker
{
	struct PlayerInput final 
	{
	public:
		sf::Vector2i inputAxes;
		bool shoot;
	};
}

#endif // !PICKER_MA1_PROGRAMMING_THREADING_PLAYERINPUT_HPP