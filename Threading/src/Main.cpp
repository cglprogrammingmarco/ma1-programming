#include <iostream>
#include <SFML/Graphics.hpp>
#include <Threading/FrequencyThread.hpp>
#include <Game/Game.hpp>

int main(int agrc, char* argv[])
{
	sf::RenderWindow window(sf::VideoMode(1920, 1080), "Ponk", sf::Style::Fullscreen);
	window.setActive(false);

	picker::Game game(window);
	game.start();

	bool isRunning = true;
	while (isRunning && window.isOpen())
	{
		sf::Event windowEvent;
		while (window.pollEvent(windowEvent))
		{
			switch (windowEvent.type)
			{
				case sf::Event::KeyPressed:
				{
					if (windowEvent.key.code == sf::Keyboard::Key::Escape)
					{
						isRunning = false;
						game.requestStop();
						window.close();
					}
					break;
				}
				case sf::Event::Closed:
				{
					isRunning = false;
					game.requestStop();
					window.close();
					break;
				}
				default:
				{
					break;
				}
			}
		}

		isRunning = game.isRunning();
	}

	return 0;
}