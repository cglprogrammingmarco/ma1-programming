#ifndef PICKER_MA1_PROGRAMMING_THREADING_SHARED_OBJECT_HPP
#define PICKER_MA1_PROGRAMMING_THREADING_SHARED_OBJECT_HPP

#include <mutex>

namespace picker
{
	template<typename T>
	class SharedObject
	{
	public:
		SharedObject() = default;
		SharedObject(const T& object);

		void set(const T& value);
		T& get();
		const T& get() const;

	private:
		T _value;
		mutable std::mutex _mutex;
	};
}

template<typename T>
picker::SharedObject<T>::SharedObject(const T& object)
{
	std::lock_guard<std::mutex> lock(_mutex);
	_value = object;
}

template<typename T>
void picker::SharedObject<T>::set(const T& value)
{
	std::lock_guard<std::mutex> lock(_mutex);
	_value = value;
}

template<typename T>
T& picker::SharedObject<T>::get()
{
	std::lock_guard<std::mutex> lock(_mutex);
	return _value;
}

template<typename T>
const T& picker::SharedObject<T>::get() const
{
	std::lock_guard<std::mutex> lock(_mutex);
	return _value;
}

#endif // !PICKER_MA1_PROGRAMMING_THREADING_SHARED_OBJECT_HPP