#include <Threading/FrequencyThread.hpp>
#include <chrono>

picker::FrequencyThread::FrequencyThread(std::function<void(float)> functionToExecute, float frequencyInHertz) :
	_functionToExecute(functionToExecute),
	_milliSecondsBetweenCalls(1.0f / frequencyInHertz * 1000),
	_isRunning(false)
{
}

picker::FrequencyThread::~FrequencyThread()
{
	_isRunning = false;
	_internalThread.join();
}

void picker::FrequencyThread::start()
{
	_isRunning = true;
	_internalThread = std::thread(&FrequencyThread::threadMainLoop, this);
}

void picker::FrequencyThread::requestStop()
{
	_isRunning = false;
}

void picker::FrequencyThread::threadMainLoop()
{
	long long lastCallTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();;

	while(_isRunning)
	{
		// Get time since Epoch in milliseconds.
		long long currentTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
		long long timeSinceLastCall = currentTime - lastCallTime;

		bool callIsdue = timeSinceLastCall >= _milliSecondsBetweenCalls;
		if (callIsdue == false)
		{
			std::this_thread::sleep_for(std::chrono::microseconds(0));
			continue;
		}

		float secondsSinceLastCall = timeSinceLastCall / 1000.0f;

		_functionToExecute(secondsSinceLastCall);
		lastCallTime = currentTime;
		std::this_thread::sleep_for(std::chrono::microseconds(0));
	}
}
