#ifndef PICKER_MA1_PROGRAMMING__THREADING_FREQUENCY_THREAD_HPP
#define PICKER_MA1_PROGRAMMING__THREADING_FREQUENCY_THREAD_HPP

#include <thread>
#include <functional>

namespace picker
{
	class FrequencyThread
	{
	public:
		FrequencyThread(std::function<void(float)> functionToExecute, float frequencyInHertz);
		~FrequencyThread();
	
		void start();
		void requestStop();
	
	private:
		std::thread _internalThread;
		std::function<void(float)> _functionToExecute;
		long long _milliSecondsBetweenCalls;
		bool _isRunning;
	
		void threadMainLoop();
	};
}

#endif // !PICKER_MA1_PROGRAMMING__THREADING_FREQUENCY_THREAD_HPP